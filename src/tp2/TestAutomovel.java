/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package tp2;

/**
 *
 * @author fabio
 */
public class TestAutomovel {

    public static void main(String[] args) {
        //a
        Automovel a1 = new Automovel("11-11-AA", "Toyota", 1400);
        //b
        System.out.println(a1.toString());
        //c
        System.out.println(a1.getMatricula());
        //d
        a1.setCilindrada(1800);
        //e
        System.out.println(a1);
        //f
        System.out.println("Automoveis existentes: " + Automovel.getnAutomoveis());
        //g
        Automovel a2 = new Automovel();
        a2.setMarca("Audi");
        a2.setMatricula("22-22-BB");
        //h
        System.out.println(a2);
        //i
        System.out.println("Automoveis existentes: " + Automovel.getnAutomoveis());
        //j
        a2.setCilindrada(-2000);
        //k
        System.out.println(a2);
        //l
        a2.setCilindrada(2000);
        //m
        System.out.println(a2);
        //n
        System.out.println("Diferença de cilindrada entre a1 e a2: "
                + a1.diferenca(a2));
        //o
        System.out.print("Matrícula do automovel com mais cilindrada: ");
        if (a1.isSuperior(a2)) {
            System.out.println(a1.getMatricula() + ".");
        } else {
            System.out.println(a2.getMatricula() + ".");
        }

        //tp2
        //4
        System.out.println("\nTP2\n");
        //b
        a1.setDataRegisto(3, 2, 2010);
        System.out.println(a1);
        //c
        a2.setDataRegisto(13, 21, 2000);
        System.out.println(a2);
        //e
        a2.setDataRegisto(13, 12, 2000);
        System.out.println(a2);

        //5
        System.out.println("\n");
        //a
        Automovel[] standAutomoveis = new Automovel[10];
        //b
        standAutomoveis[0] = a1;
        standAutomoveis[1] = a2;
        
        standAutomoveis[3] = new Automovel("AA-23-45", "Mercedes", 2000);
        standAutomoveis[3].setDataRegisto(13, 12, 2000);
        //c
        System.out.println("Número de automóveis: " + contarAutomoveis(standAutomoveis));
        //d
        System.out.println("Mostra automóveis:");
        mostrarAutomoveis(standAutomoveis);
        //e
        System.out.println("Automóveis com maior cilíndrada:");
        mostrarAutMaiorCil(standAutomoveis);
        //f
        System.out.println("Automóveis mais recentes:");
        mostrarAutRecentes(standAutomoveis);
        //g
        System.out.println("Automóveis mais antigos:");
        mostrarAutAntigos(standAutomoveis);
    }

    private static int contarAutomoveis(Automovel[] standAutomoveis) {
        int qtAutomoveis = 0;
        for (int i = 0; i < 10; i++) {
            if (standAutomoveis[i] != null) {
                qtAutomoveis++;
            }
        }
        return qtAutomoveis;
    }

    private static void mostrarAutomoveis(Automovel[] standAutomoveis) {
        if (contarAutomoveis(standAutomoveis) <= 0) {
            System.out.println("Vazio.");
            return;
        }
        for (int i = 0; i < 10; i++) {
            if (standAutomoveis[i] != null) {
                System.out.println(standAutomoveis[i]);
            }
        }
    }

    private static void mostrarAutMaiorCil(Automovel[] standAutomoveis) {
        Automovel aut = autMaiorCilindrada(standAutomoveis);
        imprimeAutsMaiorCilindrada(standAutomoveis, aut);
    }
    
        private static Automovel autMaiorCilindrada(Automovel[] standAutomoveis) {
        Automovel maiorCilin = null;
        for (int i = 0; i < 10; i++) {
            if (standAutomoveis[i] != null) {
                if (maiorCilin == null) {
                    maiorCilin = standAutomoveis[i];
                }
                if (standAutomoveis[i].isSuperior(maiorCilin)) {
                    maiorCilin = standAutomoveis[i];
                }
            }
        }
        return maiorCilin;
    }

    private static void imprimeAutsMaiorCilindrada(Automovel[] standAutomoveis, Automovel aut) {
        if (contarAutomoveis(standAutomoveis) == 0) {
            System.out.println("Vazio.");
            return;
        }
        for (int i = 0; i < 10; i++) {
            if (standAutomoveis[i] != null) {
                if (standAutomoveis[i].diferenca(aut) == 0) {
                    System.out.println(standAutomoveis[i]);
                }
            }
        }
    }

    private static void mostrarAutRecentes(Automovel[] standAutomoveis) {
        Automovel aut = autMaisRecente(standAutomoveis);
        imprimeAutData(standAutomoveis, aut);

    }

    private static Automovel autMaisRecente(Automovel[] standAutomoveis) {
        Automovel maisRecente = null;
        for (int i = 0; i < 10; i++) {
            if (standAutomoveis[i] != null) {
                if (maisRecente == null) {
                    maisRecente = standAutomoveis[i];
                }
                if (standAutomoveis[i].getDataRegisto().maior(maisRecente.getDataRegisto())) {
                    maisRecente = standAutomoveis[i];
                }
            }
        }
        return maisRecente;
    }

    private static void imprimeAutData(Automovel[] standAutomoveis, Automovel aut) {
        if (contarAutomoveis(standAutomoveis) == 0) {
            System.out.println("Vazio");
            return;
        }
        for (int i = 0; i < 10; i++) {
            if (standAutomoveis[i] != null) {
                if (standAutomoveis[i].getDataRegisto().isIgual(aut.getDataRegisto())) {
                    System.out.println(standAutomoveis[i]);
                }
            }
        }
    }

    private static void mostrarAutAntigos(Automovel[] standAutomoveis) {
        Automovel aut = autMaisAntigo(standAutomoveis);
        imprimeAutData(standAutomoveis, aut);
    }

    private static Automovel autMaisAntigo(Automovel[] standAutomoveis) {
        Automovel maisAntigo = null;
        for (int i = 0; i < 10; i++) {
            if (standAutomoveis[i] != null) {
                if (maisAntigo == null) {
                    maisAntigo = standAutomoveis[i];
                }
                if (!standAutomoveis[i].getDataRegisto().maior(maisAntigo.getDataRegisto())) {
                    maisAntigo = standAutomoveis[i];
                }
            }
        }
        return maisAntigo;
    }
}
