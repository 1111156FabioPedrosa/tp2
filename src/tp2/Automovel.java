/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package tp2;

/**
 *
 * @author fabio
 */
public class Automovel {

    private String matricula;
    private String marca;
    private int cilindrada;
    private Data dataRegisto;
    private static int nAutomoveis = 0;

    Automovel() {
        this("sem matrícula", "sem marca", 1000);
    }

    public Automovel(String matricula, String marca, int cilindrada) {
        this(matricula, marca, cilindrada, new Data());
    }

    public Automovel(String matricula, String marca, int cilindrada, Data dataRegisto) {
        setMatricula(matricula);
        setMarca(marca);
        setCilindrada(cilindrada);
        this.dataRegisto = new Data(dataRegisto);
        nAutomoveis++;
    }

    public String getMatricula() {
        return matricula;
    }

    public String getMarca() {
        return marca;
    }

    public int getCilindrada() {
        return cilindrada;
    }

    public Data getDataRegisto() {
        return dataRegisto;
    }

    public void setMatricula(String matricula) {
        this.matricula = matricula;
    }

    public void setMarca(String marca) {
        this.marca = marca;
    }

    public void setCilindrada(int cilindrada) {
        if (cilindrada > 0) {
            this.cilindrada = cilindrada;
        }
    }

    public void setDataRegisto(int dia, int mes, int ano) {
        this.dataRegisto = new Data(ano, mes, dia);
    }

    public int diferenca(Automovel aut) {
        return Math.abs(cilindrada - aut.getCilindrada());
    }

    public boolean isSuperior(Automovel aut) {
        if (cilindrada > aut.getCilindrada()) {
            return true;
        } else {
            return false;
        }
    }

    public static int getnAutomoveis() {
        return nAutomoveis;
    }

    @Override
    public String toString() {
        return "Automóvel com matrícula " + matricula + " é um " + marca
                + " e tem cilindrada de " + cilindrada + " cc,"
                + " data de registo: " + this.dataRegisto;
    }
}
